#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int seed, string bases, int n)
{
	mt19937 eng1(seed);
	string ans;
	const int min = 0;
	int max = bases.size() - 1;
	

	uniform_int_distribution<> unifrm(min,max);

	for (int i=1; i <= n; i++)
	{
		ans += bases[unifrm(eng1)];
	}

return ans;
}
